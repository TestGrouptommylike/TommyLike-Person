#include "stdafx.h"
#include "DataOperation.h"


DataOperation::DataOperation(void)
{
	CppSQLite3DB mdb;
	try
	{
		mdb.open("single.db");
		//Create Default Database
		if(!mdb.tableExists("FILES"))
		{
			mdb.execDML("CREATE TABLE FILES(fileid varchar(40), \
			ordn varchar(32),file_ext varchar(16),file_size int,\
			state int,file_name varchar(256),page_count int,print_copy int,print_time varchar(16));");
		}
		if(!mdb.tableExists("ORDERS"))
		{
			mdb.execDML("create table ORDERS(empno int, empname char(20));");
		}
		mdb.close();
	}catch(CppSQLite3Exception &e)
	{
		//cout << "WriteLockThreadProc: " << e.errorCode() << ":" << e.errorMessage() << endl;
	}	
}


DataOperation::~DataOperation(void)
{
}
void DataOperation::InsertFileInfos(std::list<FileInfo> files)
{
	CppSQLite3DB mdb;
	CppSQLite3Buffer buffer;
	mdb.open("single.db");
	mdb.execDML("begin transaction;");
	try
	{		
		std::list<FileInfo>::iterator it;
		for(it = files.begin;it != files.end;it++)
		{
			buffer.format("INSERT INTO FILES(fileid , \
			ordn ,file_ext ,file_size ,state ,filename ,\
			page_count ,print_copy,print_time) values(%Q ,\
			%Q ,%Q ,%Q ,%Q ,%Q ,%Q ,%Q ,%Q );",
			it->file_id,it->order_id,it->file_ext,it->file_size,
			it->state,it->file_name,it->page_count,it->print_count,it->print_time);
			mdb.execDML(buffer);
		}
		mdb.execDML("commit transaction;");
		
	}catch(CppSQLite3Exception &e)
	{
		mdb.execDML("rollback;");
		throw;
	}	
	mdb.close();
}
void DataOperation::UpdateFileStatus(std::string fileid,int status)
{
	CppSQLite3DB mdb;
	CppSQLite3Buffer buffer;
	mdb.open("single.db");

	try
	{
		buffer.format("update FILES set state = %Q where fileid Like %Q",status,fileid);
		mdb.execDML(buffer);
		if(true)
		{
			buffer.format("update FILES set state = %Q where fileid Like %Q",status,fileid);
			mdb.execDML(buffer);
		}

		
	}catch(CppSQLite3Exception &e)
	{		
		throw;
	}	
	mdb.close();
}

