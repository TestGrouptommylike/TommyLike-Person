#pragma once
#include "CppSQlite3.h"
#include <list>
class FileInfo
{
public:
	char* file_id;
	char* file_name;
	char* file_size;
	int   file_ext;
	int   state;
	char* order_id;
	int   page_count;
	int   print_count;
	char* print_time;
};
class OrderInfo
{

};
class DataOperation
{
private:	
public:
	DataOperation(void);
	~DataOperation(void);
	void DataOperation::InsertFileInfos(std::list<FileInfo> files);
	void DataOperation::UpdateFileStatus(std::string fileid,int status);
};

