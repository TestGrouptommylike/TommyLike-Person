#pragma once

#include <string>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/noncopyable.hpp>

#include <Loki/Singleton.h>

// Log4Cxx
#include <log4cxx/logger.h>
#include <log4cxx/xml/domconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/helpers/pool.h>
#include <log4cxx/fileappender.h>
#include <log4cxx/simplelayout.h>
using namespace log4cxx;
using namespace log4cxx::helpers;
EXTERN_C IMAGE_DOS_HEADER __ImageBase;

/**
@class LevelLogger
 
@brief Provide log capability

@note 
*/
class LevelLogger
{
    /**
    @class Inner Mutex for LevelLogger
     
    @brief 
    
    @note 
    */

    template<typename T>
    struct Guard : boost::noncopyable
    {
        Guard(T& val) : m_guardRef(val)
        {
            m_guardRef.acquire();
        }

        ~Guard(){
            m_guardRef.release();
        }

    private:
        T& m_guardRef;
    };

public:

	LevelLogger(void)
		
	{	
		LPTSTR  strDLLPath1 = new TCHAR[_MAX_PATH];
		::GetModuleFileName((HINSTANCE)&__ImageBase, strDLLPath1, _MAX_PATH);
		std::wstring fileName = strDLLPath1;
		boost::filesystem::wpath filePath(fileName);
		std::wstring modulePath = filePath.parent_path().directory_string();
		CString propertyfile = boost::str(boost::wformat(L"%1%\\%2%") % modulePath % L"log4cxx.properties").c_str();			
		PropertyConfigurator::configure(propertyfile.AllocSysString());
		logger_ = Logger::getRootLogger();
	}

	~LevelLogger(void)
	{

	}

    // 输出等级：trace < debug < info < warn < error < fatal
    void trace(const std::wstring& message)
    {
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_TRACE(logger_, message);
#endif
    }

    void trace(const std::string& message)
    {
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_TRACE(logger_, message);
#endif
    }


    void trace(const boost::wformat& message)
    {
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_TRACE(logger_, boost::str(message));
#endif
    }

    void trace(const boost::format& message)
    {
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_TRACE(logger_, message);
#endif
    }

	void debug(const boost::wformat& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_DEBUG(logger_, boost::str(message));
#endif
	}

	void debug(const boost::format& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_DEBUG(logger_, message);
#endif
	}

	void debug(const std::wstring& message)
	{
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_DEBUG(logger_, message);
#endif
	}

	void debug(const std::string& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_DEBUG(logger_, message);
#endif
	}

	void info(const std::wstring& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_INFO(logger_, message);
#endif
	}

	void info(const std::string& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_INFO(logger_, message);
#endif
	}

	void info(const boost::wformat& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_INFO(logger_, boost::str(message));
#endif
	}

	void info(const boost::format& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_INFO(logger_, message);
#endif
	}


	void warn(const std::wstring& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_WARN(logger_, message);
#endif
	}

	void warn(const std::string& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_WARN(logger_, message);
#endif
	}


	void warn(const boost::wformat& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_WARN(logger_, boost::str(message));
#endif
	}

	void warn(const boost::format& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_WARN(logger_, message);
#endif
	}


	void error(const std::wstring& message)
	{
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_ERROR(logger_, message);
#endif
	}

	void error(const std::string& message)
	{
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_ERROR(logger_, message);
#endif
	}


	void error(const boost::wformat& message)
	{
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_ERROR(logger_, boost::str(message));
#endif
	}

	void error(const boost::format& message)
	{
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_ERROR(logger_, message);
#endif
	}

	void fatal(const std::wstring& message)
	{
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_FATAL(logger_, message);
#endif
	}

	void fatal(const std::string& message)
	{
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_FATAL(logger_, message);
#endif
	}


	void fatal(const boost::wformat& message)
	{
        //Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_FATAL(logger_, boost::str(message));
#endif
	}

	void fatal(const boost::format& message)
	{
       // Guard<Mutex> g(mutex_);
#ifndef GTEST_UNITTEST
        LOG4CXX_FATAL(logger_, message);
#endif
	}


private:
	log4cxx::LoggerPtr logger_;
    //Mutex mutex_;
};

typedef Loki::SingletonHolder<LevelLogger> LOG;
