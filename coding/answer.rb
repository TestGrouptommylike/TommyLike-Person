require 'rubygems'
require 'celluloid'

class Waiter
  include Celluloid
  FREE = 0
  USED = 1
  MAX_DINNER = 10
  attr_reader :philosophers
  attr_reader :forks
  attr_reader :eating

  def initialize(forks)
    @philosophers = []
    @eating = Array.new(forks, MAX_DINNER)
    @forks = Array.new(forks, FREE)
  end

  def task_seat(philosopher)
    @philosophers << philosopher
    philosopher.async.think
  end

  def request_fork(philosopher)
    pos = @philosophers.index(philosopher)
    left_pos = pos
    right_pos = (pos + 1) % @forks.size
    if @forks[left_pos] == FREE && @forks[right_pos] == FREE
      @forks[left_pos] = USED
      @forks[right_pos] = USED
      philosopher.async.eat
    else
       philosopher.async.think
    end
  end

  def return_fork(philosopher)
    pos = @philosophers.index(philosopher)
    left_pos = pos
    right_pos = (pos + 1) % @forks.size
    @forks[left_pos] = FREE
    @forks[right_pos] = FREE
    @eating[pos] -= 1
    philosopher.async.think if @eating[pos] != 0
  end
end

class Philosopher
  include Celluloid
  attr_reader :name
  attr_reader :waiter
  attr_reader :fulled
  attr_accessor :time

  def initialize(name, waiter)
    @name = name
    @waiter = waiter
    @fulled = true
    @time = 0
    waiter.async.task_seat(Actor.current)
  end

  def think
    p "#{@time}:第(#{name})个哲学家开始思考" if @fulled
    sleep(0.1)
    @time += 100
    p "#{time}:第(#{name})个哲学家停止思考" if @fulled
    @fulled = false
    waiter.async.request_fork(Actor.current)
  end

  def eat
    p "#{@time}:第(#{name})个哲学家开始吃饭"
    sleep(0.3)
    @time += 300
    p "#{time}:第(#{name})个哲学家停止吃饭"
    @fulled = true
    waiter.async.return_fork(Actor.current)
  end
end

names = %w{1 2 3 4 5}
waiter = Waiter.new(names.size)
philosophers = names.map { |name| Philosopher.new(name, waiter) }

# The main thread is done! Sleep forever
sleep
