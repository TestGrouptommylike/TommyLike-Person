using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;

namespace TestSimpleData
{
    public class TestItem
    {
        public String Name { set; get; }
        public String Age { set; get; }

        public byte[] Content { set; get; }
    }
    public class ItemOperator
    {
        private const string DataBaseFile = "resources.sqlite";

        private const string CreateResourcesTableString =
            "create table if not exists Resources(name varchar(200), age varchar(200),content BLOB)";
        private readonly string _resourceDatabase = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DataBaseFile);
        private SQLiteConnection _connection;
        private SQLiteCommand _command;
        public ItemOperator()
        {            
            InitializeDatabase();
        }       

        /// <summary>
        /// 创建基本数据文件及表结构
        /// </summary>
        public void InitializeDatabase()
        {
            if (!File.Exists(_resourceDatabase))
            {
                SQLiteConnection.CreateFile(_resourceDatabase);
            }
            _connection = new SQLiteConnection(string.Format("Data Source={0}",_resourceDatabase));
            _connection.Open();
            _command = new SQLiteCommand(CreateResourcesTableString, _connection);
            _command.ExecuteNonQuery();
        }

        public void AddNewItem(TestItem item)
        {
            _command.CommandText = "insert into Resources (name, age, content) values (@name,@age,@content)";
            _command.Parameters.Add("@name", DbType.String).Value = item.Name;
            _command.Parameters.Add("@age", DbType.String).Value = item.Age;
            _command.Parameters.Add("@content", DbType.Binary, item.Content.Count()).Value = item.Content;
            _command.ExecuteNonQuery();
        }

        public TestItem GetItem()
        {
           return new TestItem(); 
        }
        
    }
}

http://blog.tigrangasparian.com/2012/02/09/getting-started-with-sqlite-in-c-part-one/
