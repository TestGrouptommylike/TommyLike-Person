#document http://www.tutorialspoint.com/ruby/ruby_socket_programming.htm

class RpcLogServer
    attr_reader :port
    
    def initialize(log_controller)
        # 创建TCP服务, 随机选择端口
        @tcp_server = TCPServer.new("localhost", 0)
        
        @port = @tcp_server.addr[1]
        @log_controller = log_controller
    end
    
    def start
        puts "start receive rpc log ..."
        
        Thread.new do
            loop do
                break if @tcp_server.nil?
                
                begin
                    Thread.new(@tcp_server.accept) do |socket|
                        str = socket.read
                        puts "RPC_LOG: #{str}"
                        @log_controller << {:time => Time.now, :log => str}
                        socket.close
                    end
                rescue Exception
                end
            end
        end
    end
    
    def stop
        return if @tcp_server.nil?
        
        @tcp_server.close
        @tcp_server = nil
    end
	
    
end
