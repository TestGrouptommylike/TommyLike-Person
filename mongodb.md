# 在linux上部署mongodb

众所周知，mongodb在nosql类数据库中抢眼的表现，征服了众多的程序员，

为什么呢？ "你以为你躲在这里我就找不到你了吗？没有用的，你是那样拉风的数据库，

那忧郁的眼神、唏嘘的胡渣子、神乎其技的刀法，还有那杯dry马蒂尼，都深深地出卖了你......"

那么搭建一台mongodb先吧。


    
        
        
这里讲的算是手工安装的方法

## 动手流程

### step1: 下载压缩包

压缩包可以通过各种途径、各种工具下载，下东西有各种神器(curl、迅雷、快播.....)，
本文中下载的是mongodb-linux-x86_64-2.6.7.tgz


把下载下来的压缩包放到你想安装的路径，文中是安装在**/home/huawei/**这个路径下。


### step2: 解压缩

把下载下来的文件放到指定的目录中后，就可以解压了

```bash
cd /home/huawei
tar -zxvf mongodb-linux-x86_64-2.6.7.tgz
```

此时目录下就有了名为**mongodb-linux-x86_64-2.6.7**的目录，

为了方便，将其改名为**mongodb**

```bash
mv mongodb-linux-x86_64-2.6.7 mongodb
```

### step3: 创建必要的文件及文件夹

在目录 **/home/huawei/mongodb** 下创建data文件夹，
在其下再创建名为db的文件夹，这些文件夹用于存放数据

```bash
#已经在/home/huawei/mongodb目录下了

mkdir data
cd data
mkdir db
```

在目录 **/home/huawei/mongodb** 下创建log文件夹,这个大家一看就知，
是用来放日志的。然后再创建一个mongodb.log的文件，用于日志记录

```bash
mkdir log
cd log
touch mongodb.log
```


### step4: 编写配置文件
这里只是简单的让mongodb跑起来，配置项不是很多，后续可以根据自己的实际情况去进行配置


一样的，在 **/home/huawei/mongodb** 目录下创建一个配置文件

```bash
vim mongodb.conf
```

编辑的内容如下

```
dbpath = /home/huawei/mongodb/data
logpath = /home/huawei/mongodb/log/mongodb.log
logappend = true

port = 27017
fork = true
auth = false

pidfilepath = /home/huawei/mongodb/mongodb.pid
```

mongodb默认是没有pid文件的，必须显式地指定


mongodb的配置项可以[参考](http://docs.mongodb.org/manual/reference/configuration-options/)


### step5: 开始享用mongodb
用命令行拉起就可以使用了

```bash
#已经在/home/huawei/mongodb/bin目录下了

./mongod -f /home/huawei/mongodb/mongodb.conf
```

我们在windows端使用robomongo去连接数据库


## 进阶 

### 使用god进行监控

让mongodb实现随系统自动启动，并在其进程达到某个条件或进程崩溃时，
能够自动重启

[参考资料](https://ruby-china.org/topics/21354)

根据系统配置(如何[安装god](http://rnd-git.huawei.com/asiazhang.zhangheng/blog/blob/master/deploy/god_startup.md))


将文件放到 **/etc/god/conf.d/** 路径下


这里需要特别说明一下， 有得必有失，mongodb之所以效率高，是靠内存换来的，

是吃内存的大户，所以在实际使用中，最好限制一下进程使用的内存，使用ulimit命令


```ruby
mongodb_root = '/home/huawei/mongodb'

God.watch do |w|
	w.name = 'mongodb'
	w.dir = mongodb_root
	
	# 限制内存4G
	w.start = "ulimit -m 4194304 && /home/huawei/mongodb/bin/mongod -f #{mongodb_root}/mongodb.conf"
	w.stop = "kill -2 `#{mongodb_root}/mongodb.pid`"
	w.restart = "kill -USR2 `#{mongodb_root}/mongodb.pid`"

	w.start_grace = 8.seconds
	w.restart_grace = 8.seconds
    
    w.pid_file = "#{mongodb_root}/mongodb.pid"

    w.behavior(:clean_pid_file)


    w.start_if do |start|
    	start.condition(:process_running) do |c|
    		c.interval = 5.seconds
    		c.running = false
    	end
    end


    w.restart_if do |restart|
    	restart.condition(:cpu_usage) do |c|
    		c.above = 85.percent
    		c.times = 5
    		c.interval = 1.hour
    	end
    end

    w.lifecycle do |on|
    	on.condition(:flapping) do |c|
    		c.to_state = [:start, :restart]
    		c.times = 5
    		c.within = 5.minute
    		c.transition   = :unmonitored
      		c.retry_in     = 10.minutes
      		c.retry_times  = 5
      		c.retry_within = 2.hours
    	end
    end
end
```
